import selenium
import json
import os
from time import sleep
from datetime import datetime, timedelta
import socket
import traceback
import pycountry
from pprint import pprint
import re
import mysqlx
from ftfy import fix_text
from dotenv import load_dotenv
import traceback
import requests
import socket

OPTS = {
    'sportsList': [
        'football',
        'basketball',
        'baseball',
        'ice-hockey',
        'volleyball',
        'handball',
        'rugby',
        'american-football',
        'table-tennis',
        'darts',
        'cricket',
        'snooker',
        'futsal',
        'badminton',
        'aussie-rules',
        'beach-volleyball',
        'waterpolo',
        'floorball',
        'bandy'

    ]
}
""" """
REGIONS = [
    "ASIA",
    "CIS",
    "Korea",
    "Europe",
    "Oceania",
    "North America",
    "South America",
    "Latin America",
]
hostname = socket.gethostname()
SERVER = socket.gethostbyname(hostname)


dotenv_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), '.env')
load_dotenv(dotenv_path)
SPORTS_BASE_URL = os.environ.get("SPORTS_BASE_URL")
PROXY = os.environ.get("PROXY")
DB_HOST = os.environ.get("DB_HOST")
DB_PORT = os.environ.get("DB_PORT")
DB_USER = os.environ.get("DB_USER")
DB_PASS = os.environ.get("DB_PASS")
API_URL = os.environ.get("API_URL")
API_KEY = os.environ.get("API_KEY")

HEADERS = {
    'Content-Type': 'application/json',
    'authorization': f"API {API_KEY}",
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36'
}


def addNewEvent(self, data):
    # Connect to server on localhost
    my_session = mysqlx.get_session({
        'host': DB_HOST, 'port': DB_PORT,
        'user': DB_USER, 'password': DB_PASS
    })

    my_schema = my_session.get_schema('in4freedom')

    # Use the collection 'my_collection'
    sports_event_coll = my_schema.get_collection('sports_events')
    team_table = my_schema.get_table('teams')
    competition_table = my_schema.get_table('competitions')

    data['eventDate'] = data['eventDate']

    data['homeTeam'] = self.stringCleaner(data['homeTeam'])
    data['awayTeam'] = self.stringCleaner(data['awayTeam'])
    data['region'] = self.stringCleaner(data['region'])
    data['competition'] = self.stringCleaner(data['competition'])
    data['lastUpdated']: int(datetime.timestamp(datetime.now()))
    homeTeam = team_table.select().where('url = :url').bind(
        'url', data['homeTeamURL']).execute()
    homeTeam = homeTeam.fetch_one()
    print(homeTeam)
    if homeTeam is None:
        homeTeam = team_table.insert(['name', 'sport', 'lastUpdated', 'region', "url"]) \
            .values(data['homeTeam'], data['sport'], int(datetime.now().timestamp()), data['region'], data['homeTeamURL']).execute()
        print(homeTeam)
        homeTeam = homeTeam.get_autoincrement_value()
    else:
        homeTeam = homeTeam[0]
    print("added home")
    awayTeam = team_table.select().where('url = :url').bind(
        'url', data['awayTeamURL']).execute()
    awayTeam = awayTeam.fetch_one()
    print(awayTeam)
    if awayTeam is None:
        awayTeam = team_table.insert(['name', 'sport', 'lastUpdated', 'region', "url"]) \
            .values(data['awayTeam'], data['sport'], int(datetime.now().timestamp()), data['region'], data['awayTeamURL']).execute()

        awayTeam = awayTeam.get_autoincrement_value()
    else:
        awayTeam = awayTeam[0]
    print("added away")

    competition = competition_table.select().where('url = :url').bind(
        'url', data['competitionURL']).execute()
    competition = competition.fetch_one()
    print(competition)
    if competition is None:
        competition = competition_table.insert(['name', 'sport', 'lastUpdated', 'region', "url"]) \
            .values(data['competition'], data['sport'], int(datetime.now().timestamp()), data['region'], data['competitionURL']).execute()

        competition = competition.get_autoincrement_value()
    else:
        competition = competition[0]

    pprint(data)

    doc = {
        'eventType': data['eventType'],
        'homeTeamID': homeTeam,
        'awayTeamID': awayTeam,
        'homeTeam': data['homeTeam'],
        'awayTeam': data['awayTeam'],
        'url': data['url'],
        'status': data['status'],
        'sport': data['sport'],
        'region': data['region'],
        'eventDate': data['eventDate'],
        'competitionID': competition,
        'competition': data['competition'],
        'results': ""
    }

    result = sports_event_coll.add(doc).execute()
    my_session.close()
    print(result)


def cleanData(inputSportsEvent):
    if inputSportsEvent['sport'] and len(inputSportsEvent['sport']) < 4:
        inputSportsEvent['sport'] = inputSportsEvent['sport'].upper()
    elif inputSportsEvent['sport'] and len(inputSportsEvent['sport']) >= 4:
        inputSportsEvent['sport'] = inputSportsEvent['sport'].title()
    if inputSportsEvent['homeTeam']:
        inputSportsEvent['homeTeam'] = inputSportsEvent['homeTeam'].title()

    if inputSportsEvent['awayTeam']:
        inputSportsEvent['awayTeam'] = inputSportsEvent['awayTeam'].title()

    return inputSportsEvent


def parseData(self, jsonData):
    my_session = mysqlx.get_session({
        'host': DB_HOST, 'port': DB_PORT,
        'user': DB_USER, 'password': DB_PASS
    })

    my_schema = my_session.get_schema('in4freedom')

    # Use the collection 'my_collection'
    sports_event_coll = my_schema.get_collection('sports_events')
    for dataGame in jsonData:
        # Connect to server on localhost

        if dataGame['status']['type'] != "finished":
            url = f"{SPORTS_BASE_URL}/{dataGame['slug']}/{dataGame['customId']}"
            print(url)
            check = sports_event_coll.find('url like :param').limit(
                1).bind('param', url).execute()

            # Print document
            check = check.fetch_one()
            print(check)
            if check is not None:
                continue
            gameID = dataGame['id']
            homeTeam = dataGame['homeTeam']['name']
            awayTeam = dataGame['awayTeam']['name']
            sport = dataGame['tournament']['category']['sport']['name']
            print(homeTeam, awayTeam)
            if sport.lower() == "tennis":
                if re.search("r\d\dp\d", awayTeam, re.IGNORECASE) or re.search("r\d\dp\d", homeTeam, re.IGNORECASE) or re.search("Wsf\d", awayTeam, re.IGNORECASE) or re.search("Wsf\d", homeTeam, re.IGNORECASE):
                    print("not adding")
                    continue
            else:
                print(dataGame)

            print(dataGame)
            competitionURL = f"{SPORTS_BASE_URL}/tournament/{sport.lower()}/{dataGame['tournament']['slug']}/{dataGame['tournament']['category']['slug']}/{dataGame['tournament']['id']}"
            print(sport)
            if sport.lower() == "rugby":
                sport = dataGame['tournament']['category']['name']
                region = ""
            elif sport.lower() == "tennis":
                regionString = dataGame['tournament']['name']
                region = ""
                regionDets = pycountry.countries.get(name=regionString)
                if regionDets is None:
                    regionDets = pycountry.countries.get(alpha_3=regionString)
                else:
                    region = regionDets.name
                if regionDets is None:
                    for r in REGIONS:
                        if r in regionString:
                            region = r

                if region == "":
                    regionAbbr = dataGame['tournament']['name'].split("-")
                    if len(regionAbbr) >= 2:
                        if regionAbbr[-2] == "CRO":
                            region = "Croatia"
                        else:
                            regionDets = pycountry.countries.get(
                                alpha_3=regionString)
                            if regionDets:
                                region = regionDets.name
                    else:
                        if dataGame['tournament']['category']['name'].lower() == "other":
                            region = "World"

            else:
                region = dataGame['tournament']['category']['name']

            competition = dataGame['tournament']['name'].replace('"', "'")

            region = region.replace(" Amateur", "")

            if sport.lower() == "tennis":
                if "wta" in dataGame['tournament']['category']['name'].lower() or "itf women" in dataGame['tournament']['category']['name'].lower():
                    eventType = "Women"
                elif "atp" in dataGame['tournament']['category']['name'].lower() or "itf men" in dataGame['tournament']['category']['name'].lower():
                    eventType = "Men"
                elif dataGame['tournament']['category']['name'].lower() == "other":
                    eventType = "Any"

            else:
                if "women" in competition.lower():
                    eventType = "Women"
                elif re.search("\su\d\d", awayTeam, re.IGNORECASE) or "youth" in competition.lower():
                    eventType = "Youth"
                else:
                    eventType = "Men"

            print(eventType)
            print(competition)
            competitionID = dataGame['tournament']['id']

            # print(teamsData[2].get_attribute('innerHTML'))
            startTime = datetime.fromtimestamp(
                dataGame['startTimestamp']).strftime("%Y:%m:%d %H:%M:%S")

            fixture = f"{homeTeam} - {awayTeam}"
            homeTeamURL = f"{SPORTS_BASE_URL}/team/{sport.lower()}/{dataGame['homeTeam']['slug']}/{dataGame['homeTeam']['id']}"
            awayTeamURL = f"{SPORTS_BASE_URL}/team/{sport.lower()}/{dataGame['awayTeam']['slug']}/{dataGame['awayTeam']['id']}"
            if datetime.now() < datetime.fromtimestamp(dataGame['startTimestamp']):
                status = "Pending"
            else:
                status = "Finished"

            inputSportsEvent = cleanData({
                'eventType': eventType,
                'homeTeam': homeTeam,
                'awayTeam': awayTeam,
                'url': url,
                'status': status,
                'sport': sport,
                'region': region,
                'eventDate': dataGame['startTimestamp'],
                'startTimeText': startTime,
                'competition': competition,
                'platform': "sofascore",
                'homeTeamURL': homeTeamURL,
                'homeTeamID': dataGame['homeTeam']['id'],
                'awayTeamURL': awayTeamURL,
                'awayTeamID': dataGame['awayTeam']['id'],
                'competitionURL': competitionURL,
                'competitionID': competitionID,
            })

            addNewEvent(self, inputSportsEvent)
            print("added data")
        else:
            results = []
            print(dataGame['homeScore'])
            print(dataGame['awayScore'])
            for key, value in dataGame['homeScore'].items():
                if key == "innings":
                    if len(dataGame['awayScore'][key]) > len(dataGame['homeScore'][key]):
                        obj = dataGame['awayScore'][key]
                    else:
                        obj = dataGame['homeScore'][key]
                    for subKey, subValue in obj.items():
                        for subSubKey, subSubValue in obj[subKey].items():
                            try:
                                awayValue = dataGame['awayScore'][key][subKey][subSubKey]
                            except KeyError:
                                awayValue = 0
                            except TypeError:
                                awayValue = 0
                            try:
                                homeValue = dataGame['homeScore'][key][subKey][subSubKey]
                            except KeyError:
                                homeValue = 0
                            except TypeError:
                                homeValue = 0
                            results.append({
                                'period': f"{subKey}-{subSubKey}",
                                'home': homeValue,
                                'away': awayValue,
                            })
                elif key != "current" and key != "display" and key != "point" and dataGame['tournament']['category']['sport']['name'] != "E-sports":
                    try:
                        results.append({
                            'period': key,
                            'home': dataGame['homeScore'][key],
                            'away': dataGame['awayScore'][key],
                        })
                    except KeyError:
                        pass
            print(results)
            result = sports_event_coll.modify(f"url like '{SPORTS_BASE_URL}/{dataGame['slug']}/{dataGame['customId']}'").patch({
                'lastUpdated': int(datetime.timestamp(datetime.now())),
                'results': results,
            }).execute()

            print(result)
    my_session.close()
    print("done")
    return True


def sports_fixtures(self, task, url=None):

    def switchToTab():
        print("switching tab")
        try:
            self.drivers[self.tabs['sports_fixtures']['driver']
                         ]['driver'].switch_to.window(self.tabs['sports_fixtures']['handle'])
        except selenium.common.exceptions.NoSuchWindowException:
            self.openURL(
                "sports_fixtures", SPORTS_BASE_URL, "proxyDriver")
    print(self.tabs)

    print(self.drivers)
    try:
        if "sports_fixtures" not in self.tabs:
            print("opening")
            self.openURL(
                "sports_fixtures", SPORTS_BASE_URL, "proxyDriver")
        else:
            switchToTab()

        if task == "get_urls":
            urls = []
            for sport in OPTS['sportsList']:
                getDate = datetime.now() - timedelta(1)
                for i in range(8):
                    getDateText = getDate.strftime("%Y-%m-%d")
                    urls.append(
                        {"url": f"{SPORTS_BASE_URL}/{sport}/{getDateText}", "sport": sport, 'dateText': getDateText})
                    getDate = getDate + timedelta(1)

            return urls
        elif task == "get_data":
            self.changeIP(driverKey="proxyDriver")
            print(url)
            getDateText = url['dateText']
            sportName = url['sport']
            url = url['url']

            while True:
                try:
                    if "sports_fixtures" not in self.tabs:
                        print("opening")
                        self.openURL(
                            "sports_fixtures", SPORTS_BASE_URL, "proxyDriver")
                    else:
                        switchToTab()
                    self.drivers[self.tabs['sports_fixtures']['driver']
                                 ]['driver'].get(url)
                    break
                except selenium.common.exceptions.TimeoutException:
                    print('changing IP')
                    self.changeIP(driverKey="proxyDriver")
                    self.waitBetween(1, 4)
                except selenium.common.exceptions.WebDriverException:
                    traceback.print_exc()
                    sleep(1)
            sleep(5)
            while True:
                try:
                    while True:
                        try:
                            print(
                                f'https://api.sofascore.com/api/v1/sport/{sportName}/scheduled-events/{getDateText}')
                            self.drivers[self.tabs['sports_fixtures']['driver']
                                         ]['driver'].get(
                                f'https://api.sofascore.com/api/v1/sport/{sportName}/scheduled-events/{getDateText}')
                            games = self.drivers[self.tabs['sports_fixtures']['driver']
                                                 ]['driver'].find_element(
                                self.By.TAG_NAME, 'body').text
                            print(games)
                            games = json.loads(games)
                            try:
                                games = games['events']
                            except KeyError:
                                games = []
                                break
                            break
                        except selenium.common.exceptions.WebDriverException:
                            traceback.print_exc()
                            sleep(1)
                            if "ERR_CONNECTION_CLOSED" in traceback.format_exc():
                                return
                            self.reload(driverKey="proxyDriver")
                        except selenium.common.exceptions.NoSuchWindowException:
                            traceback.print_exc()
                            sleep(1)
                            self.reload(driverKey="proxyDriver")
                    print(sportName)
                    if sportName == 'football':
                        sleep(2)
                        print(
                            f'https://api.sofascore.com/api/v1/sport/{sportName}/scheduled-events/{getDateText}/inverse')
                        try:
                            self.drivers[self.tabs['sports_fixtures']['driver']
                                         ]['driver'].get(
                                f'https://api.sofascore.com/api/v1/sport/{sportName}/scheduled-events/{getDateText}/inverse')
                        except selenium.common.exceptions.WebDriverException:
                            traceback.print_exc()
                            self.reload(driverKey="proxyDriver")
                            self.drivers['proxyDriver'].get(
                                f'https://api.sofascore.com/api/v1/sport/{sportName}/scheduled-events/{getDateText}/inverse')
                        except selenium.common.exceptions.NoSuchWindowException:
                            traceback.print_exc()
                            self.reload(driverKey="proxyDriver")
                            self.drivers[self.tabs['sports_fixtures']['driver']
                                         ]['driver'].get(
                                f'https://api.sofascore.com/api/v1/sport/{sportName}/scheduled-events/{getDateText}/inverse')

                        gamesToAdd = self.drivers[self.tabs['sports_fixtures']['driver']
                                                  ]['driver'].find_element(
                            self.By.TAG_NAME, 'body').text
                        print(gamesToAdd)
                        gamesToAdd = json.loads(gamesToAdd)
                        games = games + gamesToAdd['events']

                    '''data = {
                        'task': 'sportsFixtures',
                        'data': json.dumps(games)
                    }
                    self.sendToParser(data)'''
                    # self.runParser(data, "sportsFixtures")
                    parseData(self, games)
                    sleep(10)
                    break
                except json.decoder.JSONDecodeError:
                    traceback.print_exc()
                    if SERVER in games:
                        raise ValueError('tor not working')
                    print('changing IP')
                    self.changeIP(driverKey="proxyDriver")
                    self.waitBetween(1, 4)
                except selenium.common.exceptions.TimeoutException:
                    traceback.print_exc()
                    if SERVER in games:
                        raise ValueError('tor not working')
                    print('changing IP')
                    self.changeIP(driverKey="proxyDriver")
                    self.waitBetween(1, 4)
    except selenium.common.exceptions.WebDriverException:
        err = traceback.format_exc()
        if "disconnected from renderer" in err:
            self.quit()
            self.loadChromedriver(
                key='standard')
            self.loadChromedriver(key='proxyDriver', opts={
                'proxy': PROXY, 'vanila': True})
            self.tabs = {}
        else:
            hostname = socket.gethostname()
            # getting the IP address using socket.gethostbyname() method
            ip_address = socket.gethostbyname(hostname)
            file = __file__.replace("\\", "\\\\")
            err = traceback.format_exc().replace("\\", "\\\\")
            query = f"""
                        mutation {{ addError(inputError: {{
                        errorTitle: "Getting Sports Fixtures",
                        machine: "{ip_address}",
                        machineName: "API",
                        errorFileName: "{file}",
                        err: "{err},
                        critical: true
                    }})}}
                    """
            print(query)

            result = requests.post(API_URL, headers=HEADERS, json={
                "query": query})
        return False
    except:
        hostname = socket.gethostname()
        # getting the IP address using socket.gethostbyname() method
        ip_address = socket.gethostbyname(hostname)
        file = __file__.replace("\\", "\\\\")
        err = traceback.format_exc().replace("\\", "\\\\")
        query = f"""
                    mutation {{ addError(inputError: {{
                        errorTitle: "Getting Sports Fixtures",
                        machine: "{ip_address}",
                        machineName: "API",
                        errorFileName: "{file}",
                        err: "{traceback.format_exec()}",
                        critical: true
                    }})}}
                """
        print(query)
        result = requests.post(API_URL, headers=HEADERS, json={
            "query": query})
        return False
