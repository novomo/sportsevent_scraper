import selenium
from datetime import datetime, timedelta, date
import pytz
import json
import mysqlx
import os
from dotenv import load_dotenv
import traceback
import requests
import socket
dotenv_path = os.path.join(os.path.dirname(__file__), '.env')
load_dotenv(dotenv_path)
PROXY = os.environ.get("PROXY")
DB_HOST = os.environ.get("DB_HOST")
DB_PORT = os.environ.get("DB_PORT")
DB_USER = os.environ.get("DB_USER")
DB_PASS = os.environ.get("DB_PASS")
API_URL = os.environ.get("API_URL")
API_KEY = os.environ.get("API_KEY")

HEADERS = {
    'Content-Type': 'application/json',
    'authorization': f"API {API_KEY}",
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36'
}


def forebet(self, task, url=None):
    def switchToTab():
        print("switching tab")
        try:
            self.drivers[self.tabs['forebet']['driver']
                         ]['driver'].switch_to.window(self.tabs['forebet']['handle'])
        except selenium.common.exceptions.NoSuchWindowException:
            self.openURL(
                "forebet", "https://www.forebet.com/", "standard")

    if "forebet" not in self.tabs:
        print("opening")
        self.openURL(
            "forebet", "https://www.forebet.com/", "standard")
    else:
        switchToTab()
    tomorrows_date_string = (
        date.today() + timedelta(days=1)).strftime("%Y-%m-%d")
    next_day_date_string = (
        date.today() + timedelta(days=2)).strftime("%Y-%m-%d")
    naive = datetime.now()
    timezone = pytz.timezone("Europe/London")
    aware1 = timezone.localize(naive)
    try:
        if task == "get_urls":

            start_time = int(datetime.strptime(f"{tomorrows_date_string} 00:00:00",
                                               "%Y-%m-%d %H:%M:%S").timestamp()) - int(aware1.utcoffset().total_seconds())

            end_time = int(datetime.strptime(f"{next_day_date_string} 00:00:00",
                                             "%Y-%m-%d %H:%M:%S").timestamp()) - int(aware1.utcoffset().total_seconds())

            return [f"https://www.forebet.com/scripts/getrs.php?ln=en&tp=1x2&in={tomorrows_date_string}&ord=0&tz=+60&tzs={start_time}&tze={end_time}"]

        elif task == "get_data":
            if "forebet" not in self.tabs:
                print("opening")
                self.openURL(
                    "forebet", "https://www.forebet.com/", "standard")
            else:
                switchToTab()

            self.drivers[self.tabs['forebet']['driver']
                         ]['driver'].get(url)

            games = self.drivers[self.tabs['forebet']['driver']
                                 ]['driver'].find_element(
                self.By.TAG_NAME, 'body').text
            # print(games)
            games = json.loads(games)

            if 'forebets_trades' not in self.state:
                self.state['forebets_trades'] = {}
            my_session = mysqlx.get_session({
                'host': DB_HOST, 'port': DB_PORT,
                'user': DB_USER, 'password': DB_PASS
            })

            my_schema = my_session.get_schema('in4freedom')

            collection = my_schema.get_collection("sports_events")
            for game in games[0]:
                # print(game)
                print(game['goalsavg'])
                eventID = self.matcher("collection", "sports_events", f"eventDate = {int(datetime.strptime(game['DATE_BAH'], '%Y-%m-%d %H:%M:%S').timestamp())  - int(aware1.utcoffset().total_seconds())} AND sport = 'Football'", {
                    'homeTeam': self.stringCleaner(game['HOST_NAME']), 'awayTeam': self.stringCleaner(game['GUEST_NAME'])}, self.stringCleaner)
                print(eventID)
                if eventID is False:
                    continue
                collection.modify(f"_id = '{eventID}'").set(
                    "home_pred_fore", int(game['host_sc_pr'])).set(
                    "away_pred_fore", int(game['guest_sc_pr'])).execute()

                event = collection.find(
                    f"_id = '{eventID.iloc[0]['_id']}'").execute()
                event = event.fetch_one()
                if "ttm_home_range_lower" in event and int(game['host_sc_pr']) >= event['ttm_home_range_lower'] and int(game['host_sc_pr']) <= event['ttm_home_range_upper'] and int(game['guest_sc_pr']) >= event['ttm_away_range_lower'] and int(game['guest_sc_pr']) <= event['ttm_away_range_upper']:
                    if f"{game['HOST_NAME']}-{game['GUEST_NAME']}-dcs" not in self.state['forebets_trades']:
                        selection_string = ""
                        for i in range(event['ttm_home_range_upper']+1):
                            for j in range(event['ttm_away_range_upper']+1):
                                if i >= event['ttm_home_range_lower'] and j >= event['ttm_away_range_lower']:
                                    selection_string = f"{selection_string} ,{i}-{j}"
                        trade_query = f"""
                                mutation {{ newTrade(inputTradeAlert: {{
                                    strategy: "Dutching Correct Score",
                                    selection: "{selection_string}",
                                    sport: "Football",
                                    event: "{game['HOST_NAME']}-{game['GUEST_NAME']}",
                                    positionType: "Back",
                                    eventID: "{eventID.iloc[0]['_id']}",
                                    starttime: {int(datetime.strptime(game['DATE_BAH'], '%Y-%m-%d %H:%M:%S').timestamp())  - int(aware1.utcoffset().total_seconds())},
                                }})}}
                            """
                        result = self.sendToApi(trade_query)
                        print(result)
                        if "errors" not in result:
                            self.state['forebets_trades'][f"{game['HOST_NAME']}-{game['GUEST_NAME']}-dcs"] = {
                                'strategy': "Dutching Correct Score",
                                'selection': selection_string,
                                'sport': "Football",
                                'event': f"{game['HOST_NAME']}-{game['GUEST_NAME']}",
                                'positionType': "Back",
                                'eventID': eventID.iloc[0]['_id'],
                                'starttime': int(datetime.strptime(game['DATE_BAH'], '%Y-%m-%d %H:%M:%S').timestamp()) - int(aware1.utcoffset().total_seconds()),

                            }
                if float(game['goalsavg']) >= 3.74:

                    if f"{game['HOST_NAME']}-{game['GUEST_NAME']}-over25" not in self.state['forebets_trades']:
                        trade_query = f"""
                                mutation {{ newTrade(inputTradeAlert: {{
                                    strategy: "Over 2.5 before 20 Minutes",
                                    selection: "Over 2.5",
                                    sport: "Football",
                                    event: "{game['HOST_NAME']}-{game['GUEST_NAME']}",
                                    positionType: "Back",
                                    eventID: "{eventID.iloc[0]['_id']}",
                                    starttime: {int(datetime.strptime(game['DATE_BAH'], '%Y-%m-%d %H:%M:%S').timestamp())  - int(aware1.utcoffset().total_seconds())},
                                }})}}
                            """
                        print(trade_query)
                        result = self.sendToApi(trade_query)
                        print(result)
                        if "errors" not in result:
                            self.state['forebets_trades'][f"{game['HOST_NAME']}-{game['GUEST_NAME']}-over25"] = {
                                'strategy': "Over 2.5 before 20 Minutes",
                                'selection': "Over 2.5",
                                'sport': "Football",
                                'event': f"{game['HOST_NAME']}-{game['GUEST_NAME']}",
                                'positionType': "Back",
                                'eventID': eventID.iloc[0]['_id'],
                                'starttime': int(datetime.strptime(game['DATE_BAH'], '%Y-%m-%d %H:%M:%S').timestamp()) - int(aware1.utcoffset().total_seconds()),

                            }

            my_session.close()

            keys_to_delete = []
            for key, value in self.state['forebets_trades'].items():
                if value['starttime'] < int((datetime.now() - timedelta(hours=1)).timestamp()):
                    keys_to_delete.append(key)
            for k in keys_to_delete:
                del self.state['forebets_trades'][k]
            self.saveState()
            return
    except selenium.common.exceptions.WebDriverException:
        err = traceback.format_exc()
        if "disconnected from renderer" in err:
            self.quit()
            self.loadChromedriver(
                key='standard')
            self.loadChromedriver(key='proxyDriver', opts={
                'proxy': PROXY, 'vanila': True})
            self.tabs = {}
        else:
            hostname = socket.gethostname()
            # getting the IP address using socket.gethostbyname() method
            ip_address = socket.gethostbyname(hostname)
            file = __file__.replace("\\", "\\\\")
            err = traceback.format_exc().replace("\\", "\\\\")
            query = f"""
                        mutation {{ addError(inputError: {{
                        errorTitle: "Getting Forebet Trades",
                        machine: "{ip_address}",
                        machineName: "API",
                        errorFileName: "{file}",
                        err: "{err},
                        critical: true
                    }})}}
                    """
            print(query)

            result = requests.post(API_URL, headers=HEADERS, json={
                "query": query})
        return False
    except:
        hostname = socket.gethostname()
        # getting the IP address using socket.gethostbyname() method
        ip_address = socket.gethostbyname(hostname)
        file = __file__.replace("\\", "\\\\")
        err = traceback.format_exc().replace("\\", "\\\\")
        query = f"""
                    mutation {{ addError(inputError: {{
                        errorTitle: "Getting Forebet Trades",
                        machine: "{ip_address}",
                        machineName: "API",
                        errorFileName: "{file}",
                        err: "{traceback.format_exec()}",
                        critical: true
                    }})}}
                """
        print(query)

        result = requests.post(API_URL, headers=HEADERS, json={
            "query": query})
        return False
