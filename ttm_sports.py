import selenium
from time import sleep
from datetime import datetime, date, timedelta
import pytz
import mysqlx
import os
from dotenv import load_dotenv
import traceback
import requests
import socket
dotenv_path = os.path.join(os.path.dirname(__file__), '.env')
load_dotenv(dotenv_path)
PROXY = os.environ.get("PROXY")
DB_HOST = os.environ.get("DB_HOST")
DB_PORT = os.environ.get("DB_PORT")
DB_USER = os.environ.get("DB_USER")
DB_PASS = os.environ.get("DB_PASS")

TTM_USER = os.environ.get("TTM_USER")
TTM_PASS = os.environ.get("TTM_PASS")
API_URL = os.environ.get("API_URL")
API_KEY = os.environ.get("API_KEY")

HEADERS = {
    'Content-Type': 'application/json',
    'authorization': f"API {API_KEY}",
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36'
}


def ttm_sports(self, task, url=None):
    def switchToTab():
        print("switching tab")
        try:
            self.drivers[self.tabs['ttm']['driver']
                         ]['driver'].switch_to.window(self.tabs['ttm']['handle'])
        except selenium.common.exceptions.NoSuchWindowException:
            self.openURL(
                "ttm", "https://www.tradingthemarket.co.uk/", "standard")

    def checkLogin():
        sleep(3)
        try:
            self.drivers[self.tabs['ttm']['driver']
                         ]['driver'].find_element(self.By.CLASS_NAME, "cc-nb-okagree").click()
        except selenium.common.exceptions.NoSuchElementException:
            pass
        if "login" not in self.drivers[self.tabs['ttm']['driver']
                                       ]['driver'].current_url:
            return

        email = self.drivers[self.tabs['ttm']['driver']
                             ]['driver'].find_element(self.By.ID, "email")
        email.send_keys(self.Keys.CONTROL, "a")
        email.send_keys(self.Keys.BACKSPACE)
        email.clear()
        self.typeText(TTM_USER, email)
        passwrd = self.drivers[self.tabs['ttm']['driver']
                               ]['driver'].find_element(self.By.ID, "password")
        passwrd.send_keys(self.Keys.CONTROL, "a")
        passwrd.send_keys(self.Keys.BACKSPACE)
        passwrd.clear()
        self.typeText(TTM_PASS, passwrd)

        # passwrd.send_keys(self.Keys.ENTER)
        self.headlessClick(
            "submit_button", self.By.CLASS_NAME, driverKey="standard")
        sleep(5)
    # try:
    if "ttm" not in self.tabs:
        print("opening")
        self.openURL(
            "ttm", "https://www.tradingthemarket.co.uk/", "standard")
    else:
        switchToTab()

    if task == "get_urls":
        urls = ["https://www.tradingthemarket.co.uk/football-shg/",
                "https://www.tradingthemarket.co.uk/football-dcs/"]
        self.drivers[self.tabs['ttm']['driver']
                     ]['driver'].get("https://www.tradingthemarket.co.uk/greyhound-stats/")
        checkLogin()
        self.drivers[self.tabs['ttm']['driver']
                     ]['driver'].get("https://www.tradingthemarket.co.uk/greyhound-stats/")
        sleep(2)
        html = self.drivers[self.tabs['ttm']['driver']
                            ]['driver'].find_element(self.By.TAG_NAME, "body").get_attribute("innerHTML")

        # turn page into soup
        main_soup = self.makeSoup(html)
        try:
            races = main_soup.find(
                "div", {'id': 'race_selection'}).find_all('a')
        except AttributeError:
            return None

        for race in races:
            print(race)
            self.drivers[self.tabs['ttm']['driver']
                         ]['driver'].get(f"https://www.tradingthemarket.co.uk{race['href']}")
            html = self.drivers[self.tabs['ttm']['driver']
                                ]['driver'].find_element(self.By.TAG_NAME, "body").get_attribute("innerHTML")

            # turn page into soup
            race_soup = self.makeSoup(html)
            for time in race_soup.find("div", {"id": "time_selection"}).find_all("a"):
                urls.append(
                    f"https://www.tradingthemarket.co.uk{time['href']}")

        return urls

    elif task == "get_data":
        try:
            todays_date_string = date.today().strftime("%Y-%m-%d")
            if 'ttm_trades' not in self.state:
                self.state['ttm_trades'] = {}

            self.drivers[self.tabs['ttm']['driver']
                         ]['driver'].get(url)

            sleep(2)
            html = self.drivers[self.tabs['ttm']['driver']
                                ]['driver'].find_element(self.By.TAG_NAME, "body").get_attribute("innerHTML")

            # turn page into soup

            checkLogin()
            main_soup = self.makeSoup(html)
            my_session = mysqlx.get_session({
                'host': DB_HOST, 'port': DB_PORT,
                'user': DB_USER, 'password': DB_PASS
            })

            my_schema = my_session.get_schema('in4freedom')
            if "greyhound-stats" in url:
                time_labels = ['distance_sectional', 'distance_actual', 'distance_calculated',
                               'distance_grade_sectional', 'distance_grade_actual', 'distance_grade_calculated',
                               'distance_trap_sectional', 'distance_trap_actual', 'distance_trap_calculated',
                               'distance_trap_grade_sectional', 'distance_trap_grade_actual', 'distance_trap_grade_calculated']
                dets = url.split("/")
                track = dets[-5]
                print(
                    f" url like '%www.timeform.com/greyhound-racing/racecards/{track.lower().replace(' ', '-')}/{dets[-4].replace(':', '')[1:] if dets[-4].replace(':', '').startswith('0') else dets[-4].replace(':', '')}/{todays_date_string}%'")

                my_collection = my_schema.get_collection("greyhound_races")
                race = my_collection.find(
                    f" url like '%www.timeform.com/greyhound-racing/racecards/{track.lower().replace(' ', '-')}/{dets[-4].replace(':', '')[1:] if dets[-4].replace(':', '').startswith('0') else dets[-4].replace(':', '')}/{todays_date_string}%'").execute()
                race = race.fetch_one()
                if race is None:
                    my_session.close()
                    return
                dog_collection = my_schema.get_table("greyhounds")
                for runner in race['runners']:
                    print(runner)
                    if "greyhoundID" not in runner:
                        runner['name'] = runner['greyhound']['name']
                    else:
                        dog = dog_collection.select().where(
                            f"greyhoundID = '{runner['greyhoundID']}'").execute()
                        dog = dog.fetch_one()
                        print(dog)
                        runner['name'] = dog[2]
                print(race)
                green = "rgb(34, 136, 11)"
                dark_red = "rgb(161, 11, 11)"
                light_red = "rgb(192, 47, 47)"

                dog_rows = main_soup.find_all("tr", {"class": "dog_row"})

                new_runners = []
                for dog_row in dog_rows:
                    tds = dog_row.find_all("td")
                    dog_name = tds[2].text
                    print(dog_name)
                    runner = next(
                        (item for item in race['runners'] if dog_name.lower() in item["name"].lower()), None)
                    if runner is None:
                        continue

                    times = dog_row.find_all("td", {"class": "time"})
                    greens = 0
                    light_reds = 0
                    dark_reds = 0

                    # print(times)
                    for i, time in enumerate(times):
                        print(time)
                        try:
                            runner[time_labels[i]] = float(time.text)
                        except ValueError:
                            runner[time_labels[i]] = 1000
                        try:
                            style = time['style']
                        except KeyError:
                            continue
                        if green in style:
                            greens = greens + 1
                        elif light_red in style:
                            light_reds = light_reds + 1
                        elif dark_red in style:
                            dark_reds = dark_reds + 1
                    new_runners.append(runner)
                    print(greens)
                    print((1/12)*greens)
                    print(dark_reds)
                    if (1/12)*greens > 0.8 and dark_reds == 0:
                        naive = datetime.now()
                        timezone = pytz.timezone("Europe/London")
                        aware1 = timezone.localize(naive)
                        if f'{track}-{dog_name}' not in self.state['ttm_trades']:
                            trade_query = f"""
                                mutation {{newTrade(inputTradeAlert: {{
                                    eventURL: "https://www.timeform.com/greyhound-racing/racecards/{track.lower().replace(' ', '-')}/{dets[-4].replace(':', '')[1:] if dets[-4].replace(':', '').startswith('0') else dets[-4].replace(':', '')}/{todays_date_string}/",
                                    strategy: "Back Greys",
                                    selection: "{dog_name}",
                                    sport: "Greyhound Racing",
                                    positionType: "Back",
                                    event: "{track}",
                                    starttime: {int(datetime.strptime(f"{todays_date_string} {dets[-4]}", "%Y-%m-%d %H:%M").timestamp()) - int(aware1.utcoffset().total_seconds())},
                                }})}}
                            """
                            print(trade_query)
                            result = self.sendToApi(trade_query)
                            print(result)
                            if "errors" not in result:
                                self.state['ttm_trades'][f'{track}-{dog_name}'] = {
                                    'strategy': "Back Greys",
                                    'selection': dog_name,
                                    'sport': "Greyhound Racing",
                                    'positionType': "Back",
                                    'event': track,
                                    'starttime': int(datetime.strptime(f"{todays_date_string} {dets[-4]}", "%Y-%m-%d %H:%M").timestamp()) - int(aware1.utcoffset().total_seconds()),
                                }
                print(new_runners)
                try:
                    my_collection.modify(f"_id = '{race['_id']}'").patch(
                        {'runners': new_runners}).execute()
                except mysqlx.errors.InterfaceError:
                    my_session.close()
                    sleep(2)
                    my_session = mysqlx.get_session({
                        'host': DB_HOST, 'port': DB_PORT,
                        'user': DB_USER, 'password': DB_PASS
                    })

                    my_schema = my_session.get_schema('in4freedom')
                    my_collection = my_schema.get_collection("greyhound_races")
                    dog_collection = my_schema.get_collection("greyhounds")
                    my_collection.modify(f"_id = '{race['_id']}'").patch(
                        {'runners': new_runners}).execute()
                my_session.close()
            if "football-shg" in url:
                naive = datetime.now()
                timezone = pytz.timezone("Europe/London")
                aware1 = timezone.localize(naive)
                games = main_soup.find_all("tr", {"class": "fixtures"})
                for game in games:
                    tds = game.find_all("td")
                    shg = float(game.find("td", {"class": "avg_shg"}
                                          ).text.replace("%", ""))
                    over_05 = float(game.find(
                        "td", {"class": "avg_over05"}).text.replace("%", ""))
                    over_15 = float(game.find(
                        "td", {"class": "avg_over15"}).text.replace("%", ""))
                    over_25 = float(game.find(
                        "td", {"class": "avg_over25"}).text.replace("%", ""))
                    over_35 = float(game.find(
                        "td", {"class": "avg_over35"}).text.replace("%", ""))

                    if shg >= 60 and over_05 >= 90 and over_15 >= 90 and over_25 >= 60 and over_35 >= 50:
                        fixture = tds[3].text.split(" v ")
                        fixture_timestamp = int(game['data-fixture_date_time'])
                        eventID = self.matcher("collection", "sports_events", f"eventDate = {fixture_timestamp - int(aware1.utcoffset().total_seconds())} AND sport = 'Football'", {
                            'homeTeam': self.stringCleaner(fixture[0]), 'awayTeam': self.stringCleaner(fixture[1])}, self.stringCleaner)
                        trade_query = f"""
                                mutation {{ newTrade(inputTradeAlert: {{
                                    strategy: "Over 2.5 before 20 Minutes",
                                    selection: "Over 2.5",
                                    sport: "Football",
                                    positionType: "Back",
                                    eventID: "{eventID}",
                                    starttime: {fixture_timestamp - int(aware1.utcoffset().total_seconds())},
                                }})}}
                            """
                        result = self.sendToApi(trade_query)
                        print(result)
                        if "errors" not in result:
                            self.state['ttm_trades'][f"{'-'.join(fixture)}-{fixture_timestamp}-shg"] = {
                                'strategy': "Over 2.5 before 20 Minutes",
                                'selection': "Over 2.5",
                                'sport': "Football",
                                'positionType': "Back",
                                'eventID': eventID,
                                'starttime': fixture_timestamp - int(aware1.utcoffset().total_seconds()),
                            }
            if "fooball-dcs" in url:
                naive = datetime.now()
                timezone = pytz.timezone("Europe/London")
                aware1 = timezone.localize(naive)

                collection = my_schema.get_collection("sports_events")
                games = main_soup.find_all("tr", {"class": "fixtures"})
                for game in games:
                    if float(game.find("td", {"class": "sd_home"}).text) < 0.8 and float(game.find("td", {"class": "sd_away"}).text) < 0.8:
                        tds = game.find_all("td")
                        fixture = tds[3].text.split(" v ")
                        fixture_timestamp = int(game['data-fixture_date_time'])

                        home_range = tds[7].split(" - ")
                        away_range = tds[-3].split(" - ")
                        ttm_home_range_lower = int(home_range[0])
                        ttm_home_range_upper = int(home_range[1])
                        ttm_away_range_lower = int(away_range[0])
                        ttm_away_range_upper = int(away_range[1])
                        print((ttm_home_range_upper-ttm_home_range_lower+1)
                              * (ttm_away_range_upper-ttm_away_range_lower+1))
                        if (ttm_home_range_upper-ttm_home_range_lower+1) * (ttm_away_range_upper-ttm_away_range_lower+1) > 6:
                            continue
                        eventID = self.matcher("collection", "sports_events", f"eventDate = {fixture_timestamp - int(aware1.utcoffset().total_seconds())} AND AND sport = 'Football'", {
                            'homeTeam': self.stringCleaner(fixture[0]), 'awayTeam': self.stringCleaner(fixture[1])}, self.stringCleaner)

                        event = collection.get_one(eventID)

                        print(event)

                        if "home_pred_fore" in event and int(event['host_sc_pr']) >= ttm_home_range_lower and int(event['host_sc_pr']) <= ttm_home_range_upper and int(event['guest_sc_pr']) >= ttm_away_range_lower and int(event['guest_sc_pr']) <= ttm_away_range_upper:
                            selection_string = ""
                            for i in range(ttm_home_range_upper+1):
                                for j in range(ttm_away_range_upper+1):
                                    if i >= ttm_home_range_lower and j >= ttm_away_range_lower:
                                        selection_string = f"{i}-{j}"
                                        trade_query = f"""
                                                mutation {{ newTrade(inputTradeAlert: {{
                                                    strategy: "Dutching Correct Score",
                                                    selection: "{selection_string}",
                                                    sport: "Football",
                                                    positionType: "Back",
                                                    eventID: "{eventID}",
                                                    starttime: {int(datetime.strptime(game['DATE_BAH'], '%Y-%m-%d %H:%M:%S').timestamp())  - int(aware1.utcoffset().total_seconds())},
                                                }})}}
                                            """
                            result = self.sendToApi(trade_query)
                            print(result)
                            if "errors" not in result:
                                self.state['ttm_trades'][f"{'-'.join(fixture)}-{fixture_timestamp}-dcs"] = {
                                    'strategy': "Dutching Correct Score",
                                    'selection': selection_string,
                                    'sport': "Football",
                                    'positionType': "Back",
                                    'eventID': eventID,
                                    'starttime': int(datetime.strptime(game['DATE_BAH'], '%Y-%m-%d %H:%M:%S').timestamp()) - int(aware1.utcoffset().total_seconds()),

                                }
                my_session.close()
            sleep(4)

            keys_to_delete = []
            for key, value in self.state['ttm_trades'].items():
                if value['starttime'] + 86400 < int((datetime.now() - timedelta(hours=1)).timestamp()):
                    keys_to_delete.append(key)
            for k in keys_to_delete:
                del self.state['ttm_trades'][k]
            self.saveState()
        except:
            hostname = socket.gethostname()
            # getting the IP address using socket.gethostbyname() method
            ip_address = socket.gethostbyname(hostname)
            query = f"""
                        mutation {{ addError(inputError: {{
                            errorTitle: "Getting TTM Data",
                            machine: "{ip_address}"
                            machineName: "API",
                            errorFileName: "{__file__}",
                            err: "{traceback.format_exec()}",
                            critical: true
                        }})}}
                    """

            result = requests.post(API_URL, headers=HEADERS, json={
                "query": query})
