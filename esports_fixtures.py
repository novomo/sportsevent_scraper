import selenium
from datetime import datetime, timedelta, date
import pytz
import json
from dotenv import load_dotenv
import os
import mysqlx
from pprint import pprint
from time import sleep
import traceback
import requests
import socket


dotenv_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), '.env')
load_dotenv(dotenv_path)
ESPORTS_BASE_URL = os.environ.get("ESPORTS_BASE_URL")
PROXY = os.environ.get("PROXY")
DB_HOST = os.environ.get("DB_HOST")
DB_PORT = os.environ.get("DB_PORT")
DB_USER = os.environ.get("DB_USER")
DB_PASS = os.environ.get("DB_PASS")
API_URL = os.environ.get("API_URL")
API_KEY = os.environ.get("API_KEY")

HEADERS = {
    'Content-Type': 'application/json',
    'authorization': f"API {API_KEY}",
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36'
}


def addNewEvent(self, data):
    # Connect to server on localhost
    my_session = mysqlx.get_session({
        'host': DB_HOST, 'port': DB_PORT,
        'user': DB_USER, 'password': DB_PASS
    })

    my_schema = my_session.get_schema('in4freedom')

    # Use the collection 'my_collection'
    sports_event_coll = my_schema.get_collection('esports_events')
    team_table = my_schema.get_table('teams')
    competition_table = my_schema.get_table('competitions')

    data['eventDate'] = data['eventDate']

    data['homeTeam'] = self.stringCleaner(data['homeTeam'])
    data['awayTeam'] = self.stringCleaner(data['awayTeam'])
    data['region'] = self.stringCleaner(data['region'])
    data['competition'] = self.stringCleaner(data['competition'])
    data['lastUpdated'] = int(datetime.timestamp(datetime.now()))
    homeTeam = team_table.select().where('url = :url').bind(
        'url', data['homeTeamURL']).execute()
    homeTeam = homeTeam.fetch_one()
    print(homeTeam)
    if homeTeam is None:
        homeTeam = team_table.insert(['name', 'sport', 'lastUpdated', 'region', "url"]) \
            .values(data['homeTeam'], data['sport'], int(datetime.now().timestamp()), data['region'], data['homeTeamURL']).execute()
        print(homeTeam)
        homeTeam = homeTeam.get_autoincrement_value()
    else:
        homeTeam = homeTeam[0]
    print("added home")
    awayTeam = team_table.select().where('url = :url').bind(
        'url', data['awayTeamURL']).execute()
    awayTeam = awayTeam.fetch_one()
    print(awayTeam)
    if awayTeam is None:
        awayTeam = team_table.insert(['name', 'sport', 'lastUpdated', 'region', "url"]) \
            .values(data['awayTeam'], data['sport'], int(datetime.now().timestamp()), data['region'], data['awayTeamURL']).execute()

        awayTeam = awayTeam.get_autoincrement_value()
    else:
        awayTeam = awayTeam[0]
    print("added away")

    competition = competition_table.select().where('url = :url').bind(
        'url', data['competitionURL']).execute()
    competition = competition.fetch_one()
    print(competition)
    if competition is None:
        competition = competition_table.insert(['name', 'sport', 'lastUpdated', 'region', "url"]) \
            .values(data['competition'], data['sport'], int(datetime.now().timestamp()), data['region'], data['competitionURL']).execute()

        competition = competition.get_autoincrement_value()
    else:
        competition = competition[0]

    pprint(data)

    doc = {
        'homeTeamID': homeTeam,
        'awayTeamID': awayTeam,
        'homeTeam': data['homeTeam'],
        'awayTeam': data['awayTeam'],
        'url': data['url'],
        'status': data['status'],
        'sport': data['sport'],
        'region': data['region'],
        'eventDate': data['eventDate'],
        'competitionID': competition,
        'competition': data['competition'],
        'results': ""
    }

    result = sports_event_coll.add(doc).execute()
    my_session.close()
    print(result)


def cleanData(inputSportsEvent):
    if inputSportsEvent['sport'] and len(inputSportsEvent['sport']) < 4:
        inputSportsEvent['sport'] = inputSportsEvent['sport'].upper()
    elif inputSportsEvent['sport'] and len(inputSportsEvent['sport']) >= 4:
        inputSportsEvent['sport'] = inputSportsEvent['sport'].title()
    if inputSportsEvent['homeTeam']:
        inputSportsEvent['homeTeam'] = inputSportsEvent['homeTeam'].title()

    if inputSportsEvent['awayTeam']:
        inputSportsEvent['awayTeam'] = inputSportsEvent['awayTeam'].title()

    return inputSportsEvent


def parseData(self, eSportsData, result, url=""):
    soup = self.makeSoup(eSportsData)
    print("soup made")
    # Connect to server on localhost
    my_session = mysqlx.get_session({
        'host': DB_HOST, 'port': DB_PORT,
        'user': DB_USER, 'password': DB_PASS
    })

    my_schema = my_session.get_schema('in4freedom')

    # Use the collection 'my_collection'
    sports_event_coll = my_schema.get_collection('esports_events')
    if result is False:

        matches = soup.find_all('div', {"class": "make_bnt_list"})
        print(matches)
        if len(matches) == 0:
            print(eSportsData)
        for match in matches:

            matchUrl = match.select(
                'a[class*="matchBox"]')[0]['href'].replace("/matches", "")
            print(match.select(
                'a[class*="matchBox"]')[0]['href'])
            print(matchUrl)
            url = f"{ESPORTS_BASE_URL.replace('/matches', '')}{matchUrl}"
            print(url)
            check = sports_event_coll.find('url like :param').limit(
                1).bind('param', url).execute()

            # Print document
            check = check.fetch_one()
            print(check)
            if check is not None:
                continue
            print(match)

            startTime = match.select(
                "span[class*='matchTime']")[0].text.strip()
            print(startTime)
            if startTime == "Live":
                continue

            homeTeam = match.select(
                'span[class*="matchTeamLeft"]')[0].select('span[class*="matchTeamName"]')[0].text.strip()
            awayTeam = match.select(
                'span[class*="matchTeamRight"]')[0].select('span[class*="matchTeamName"]')[0].text.strip()
            if homeTeam == "" or awayTeam == "" or homeTeam == "TBD" or awayTeam == "TBD":
                return

            print(match.select('span[class*="matchIcon"]')[0])
            sport = match.select(
                'span[class*="matchIcon"]')[0].find("img")['alt']
            competition = match.select(
                'span[class*="tournamentName"]')[0].text.strip().split(" - ")[0]

            region = "World"

            homeTeamURL = f"{ESPORTS_BASE_URL.replace('/matches', '')}/{sport}/team/{homeTeam.lower().replace(' ', '-')}"
            awayTeamURL = f"{ESPORTS_BASE_URL.replace('/matches', '')}/{sport}/team/{awayTeam.lower().replace(' ', '-')}"
            competitionURL = f"{ESPORTS_BASE_URL.replace('/matches', '')}/{sport}/event/{competition.lower().replace(' ', '-')}"

            print(region, competition, startTime,
                  sport, homeTeam, awayTeam)
            print(url)
            print(homeTeam, awayTeam)
            naive = datetime.now()
            timezone = pytz.timezone("Europe/London")
            aware1 = timezone.localize(naive)
            inputSportsEvent = cleanData({
                'homeTeam': homeTeam,
                'awayTeam': awayTeam,
                'url': url,
                'sport': sport,
                'region': region,
                'eventDate': int(datetime.timestamp(datetime.strptime(startTime, "%d.%m.%y %H:%M")) - int(aware1.utcoffset().total_seconds())),
                'status': "Pending",
                'startTimeText': startTime,
                'competition': competition,
                'platform': "egamersworld",
                'homeTeamURL': homeTeamURL,
                'homeTeamID': homeTeamURL,
                'awayTeamURL': awayTeamURL,
                'awayTeamID': awayTeamURL,
                'competitionURL': competitionURL,
                'competitionID': competitionURL,
            })

            addNewEvent(self, inputSportsEvent)

    else:
        print("result")
        results = []
        print(len(soup.select("div[class*='_maps']")))
        if len(soup.select("div[class*='_maps']")) > 0:
            maps = soup.select(
                "div[class*='_maps']")[0].select("div[class*='_map']")
            for map in maps:
                print(map['class'])
                if "waiting" in map['class']:
                    continue
                if "cs_map" in map['class']:
                    print({
                        'period': map.select('div[class*="map_name"]')[0].get_text(),
                        'home': map.select('div[class*="home_score"]')[0].get_text(),
                        'away': map.select('div[class*="away_score"]')[0].get_text(),
                        'pick': "home" if len(map.select('div[class*="home_pick"]')) > 0 else "away"
                    })
                    results.append({
                        'period': map.select('div[class*="map_name"]')[0].get_text(),
                        'home': map.select('div[class*="home_score"]')[0].get_text(),
                        'away': map.select('div[class*="away_score"]')[0].get_text(),
                        'pick': "home" if len(map.select('div[class*="home_pick"]')) > 0 else "away"
                    })
                elif "dota_map" in map['class']:
                    print({
                        'period': map.select('div[class*="map_name"]')[0].get_text(),
                        'home': map.select('div[class*="kills"]')[0].get_text(),
                        'away': map.select('div[class*="kills"]')[1].get_text(),
                        'winner': "home" if "win" in map.select('div[class*="team_name"]')[0]['class'] else "away"
                    })
                    results.append({
                        'period': map.select('div[class*="map_name"]')[0].get_text(),
                        'home': map.select('div[class*="kills"]')[0].get_text(),
                        'away': map.select('div[class*="kills"]')[1].get_text(),
                        'winner': "home" if "win" in map.select('div[class*="team_name"]')[0]['class'] else "away"
                    })
            print(map.select('div[class*="scoresTotal"]'))
            print({
                'period': "final",
                'home': soup.select('div[class="scoresTotal"]')[0].find_all("div")[0].get_text(),
                'away': soup.select('div[class="scoresTotal"]')[0].find_all("div")[1].get_text()
            })

            results.append({
                'period': "final",
                'home': soup.select('div[class="scoresTotal"]')[0].find_all("div")[0].get_text(),
                'away': soup.select('div[class="scoresTotal"]')[0].find_all("div")[1].get_text()
            })
        else:
            print(soup.select('div[class*="match-result"]'))
            print(soup.select('div[class*="match-result"]')
                  [0].find_all("span"))
            results.append({
                'period': "final",
                'home': soup.select('div[class*="match-result"]')[0].find_all("span")[0].get_text(),
                'away': soup.select('div[class*="match-result"]')[0].find_all("span")[1].get_text(),
            })
        result = sports_event_coll.modify(f"url like '{url}'").patch({
            'lastUpdated': int(datetime.timestamp(datetime.now())),
            'results': results,
        }).execute()

        print(result)
    my_session.close()


def esports_fixtures(self, task, url=None):
    def switchToTab():
        print("switching tab")
        try:
            self.drivers[self.tabs['esports_fixtures']['driver']
                         ]['driver'].switch_to.window(self.tabs['esports_fixtures']['handle'])
        except selenium.common.exceptions.NoSuchWindowException:
            self.openURL(
                "esports_fixtures", ESPORTS_BASE_URL, "standard")
    try:
        if "esports_fixtures" not in self.tabs:
            print("opening")
            self.openURL(
                "esports_fixtures", ESPORTS_BASE_URL, "standard")
        else:
            switchToTab()

        if task == "get_urls":

            return [ESPORTS_BASE_URL]

        elif task == "get_data":

            self.drivers[self.tabs['esports_fixtures']['driver']
                         ]['driver'].get(url)

            self.headlessWaitForElements(
                "present",  "make_bnt_list", self.By.CLASS_NAME, waitTime=10, driverKey='standard')

            html = self.drivers[self.tabs['esports_fixtures']['driver']
                                ]['driver'].find_element(
                self.By.TAG_NAME, 'body').get_attribute('innerHTML')
            parseData(self, html, False)
            return

        elif task == "results":
            startTime = datetime.timestamp(datetime.now())

            nowTimeStamp = int(datetime.timestamp(datetime.now())) - (3600*3)
            try:
                my_session = mysqlx.get_session({
                    'host': DB_HOST, 'port': DB_PORT,
                    'user': DB_USER, 'password': DB_PASS
                })
            except mysqlx.errors.InterfaceError:
                return

            my_schema = my_session.get_schema('in4freedom')

            # Use the collection 'my_collection'
            sports_event_coll = my_schema.get_collection('esports_events')

            results = sports_event_coll.find(
                f"results = '' AND eventDate < {nowTimeStamp}").execute()
            results = results.fetch_all()
            print(results)
            results.reverse()
            for result in results:
                print(result)
                if "matches" in result['url']:
                    sports_event_coll.remove_one(result['_id'])
                    continue
                while True:
                    try:
                        self.drivers[self.tabs['esports_fixtures']['driver']
                                     ]['driver'].set_page_load_timeout(15)
                        self.drivers[self.tabs['esports_fixtures']['driver']
                                     ]['driver'].get(result['url'])
                        break
                    except selenium.common.exceptions.TimeoutException:
                        pass
                    except selenium.common.exceptions.WebDriverException:
                        self.reload(driverKey="standard")
                        sleep(2)
                if "history" in self.drivers[self.tabs['esports_fixtures']['driver']
                                             ]['driver'].current_url:
                    sports_event_coll.remove_one(result['_id'])
                    continue
                if self.drivers[self.tabs['esports_fixtures']['driver']
                                ]['driver'].current_url.endswith("matches"):
                    sports_event_coll.remove_one(result['_id'])
                    continue
                while True:
                    try:
                        self.drivers[self.tabs['esports_fixtures']['driver']
                                     ]['driver'].find_element(
                            self.By.ID, 'challenge-running')
                        self.changeIP(driverKey="standard")
                        self.waitBetween(1, 4)
                        print("changing")
                    except selenium.common.exceptions.NoSuchElementException:
                        break
                matchLive = self.drivers[self.tabs['esports_fixtures']['driver']
                                         ]['driver'].find_elements(
                    self.By.CLASS_NAME, 'match_live')
                showResult = self.drivers[self.tabs['esports_fixtures']['driver']
                                          ]['driver'].find_elements(
                    self.By.XPATH, '//span[contains(@class, "show_result")]')
                print(showResult)
                print(self.drivers[self.tabs['esports_fixtures']['driver']
                                   ]['driver'].find_elements(
                    self.By.CLASS_NAME, 'scoresTotal'))
                if len(matchLive) > 0:
                    print(matchLive[0].get_attribute('innerHTML'))
                    matchDate = self.drivers[self.tabs['esports_fixtures']['driver']
                                             ]['driver'].find_element(
                        self.By.CLASS_NAME, 'match_day').get_attribute('innerHTML')
                    if matchLive[0].get_attribute('innerHTML').lower() == "live":
                        if int(datetime.timestamp(datetime.strptime(matchDate, "%d.%m.%Y"))) < int(datetime.timestamp(datetime.now())) - (2*86400):
                            sports_event_coll.remove_one(result['_id'])
                        print(datetime.timestamp(datetime.now()) - startTime)
                        if datetime.timestamp(datetime.now()) - startTime > 60:
                            return
                        continue

                if len(showResult) == 0 and len(self.drivers[self.tabs['esports_fixtures']['driver']
                                                             ]['driver'].find_elements(self.By.CLASS_NAME, 'scoresTotal')) == 0:
                    print(self.drivers[self.tabs['esports_fixtures']['driver']
                                       ]['driver'].find_element(
                        self.By.TAG_NAME, 'body').get_attribute('innerHTML'))
                    print(datetime.timestamp(datetime.now()) - startTime)
                    if datetime.timestamp(datetime.now()) - startTime > 60:

                        return
                    continue
                if len(showResult) > 0:
                    self.headlessClick(
                        '//span[contains(@class, "show_result")]', self.By.XPATH, driverKey='standard')
                data = {
                    'task': 'eSportsFixtures',
                    'data': {
                        'data': self.drivers[self.tabs['esports_fixtures']['driver']
                                             ]['driver'].find_element(self.By.TAG_NAME, 'body').get_attribute('innerHTML'),
                        'result': True
                    }
                }
                # self.sendToParser(data)
                parseData(self, self.drivers[self.tabs['esports_fixtures']['driver']
                                             ]['driver'].find_element(
                    self.By.TAG_NAME, 'body').get_attribute('innerHTML'), True, result['url'])
                print(datetime.timestamp(datetime.now()) - startTime)
                if datetime.timestamp(datetime.now()) - startTime > 60:
                    return
    except selenium.common.exceptions.WebDriverException:
        err = traceback.format_exc()
        if "disconnected from renderer" in err:
            self.quit()
            self.loadChromedriver(
                key='standard')
            self.loadChromedriver(key='proxyDriver', opts={
                'proxy': PROXY, 'vanila': True})
            self.tabs = {}
        else:
            hostname = socket.gethostname()
            # getting the IP address using socket.gethostbyname() method
            ip_address = socket.gethostbyname(hostname)
            file = __file__.replace("\\", "\\\\")
            err = traceback.format_exc().replace("\\", "\\\\")
            query = f"""
                        mutation {{ addError(inputError: {{
                        errorTitle: "Getting Esports Fixtures",
                        machine: "{ip_address}",
                        machineName: "API",
                        errorFileName: "{file}",
                        err: "{err},
                        critical: true
                    }})}}
                    """
            print(query)

            result = requests.post(API_URL, headers=HEADERS, json={
                "query": query})
        return False
    except:
        hostname = socket.gethostname()
        # getting the IP address using socket.gethostbyname() method
        ip_address = socket.gethostbyname(hostname)
        file = __file__.replace("\\", "\\\\")
        err = traceback.format_exc().replace("\\", "\\\\")
        query = f"""
                    mutation {{ addError(inputError: {{
                        errorTitle: "Getting Esports Fixtures",
                        machine: "{ip_address}",
                        machineName: "API",
                        errorFileName: "{file}",
                        err: "{traceback.format_exec()}",
                        critical: true
                    }})}}
                """
        print(query)

        result = requests.post(API_URL, headers=HEADERS, json={
            "query": query})
        return False
